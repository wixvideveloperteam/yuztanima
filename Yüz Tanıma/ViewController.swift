//
//  ViewController.swift
//  Yüz Tanıma
//
//  Created by Yıldırımhan Atçıoğlu on 23.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit
import Vision
class ViewController: UIViewController {
    @IBOutlet weak var myPhoto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        faceDetection(image: myPhoto.image!)
    }
    
    func faceDetection(image:UIImage){
        self.myPhoto.image = image
        
        guard let ciImage = CIImage(image:image) else {return}
        
        let request = VNDetectFaceRectanglesRequest { (request, error) in
            if let error = error {
                print(error)
            }
            else
            {
                self.myPhoto.layer.sublayers?.forEach({ (layer) in
                    layer.removeFromSuperlayer()
                })
                
                guard let faceObservation = request.results as? [VNFaceObservation] else {return}
                faceObservation.forEach({ (face) in
                    print(face.boundingBox)
                    let boundingBox = face.boundingBox
                    let size = CGSize(width: boundingBox.width*self.myPhoto.bounds.width, height: boundingBox.height*self.myPhoto.bounds.height)
                    let origin = CGPoint(x: boundingBox.minX*self.myPhoto.bounds.width, y: (1-face.boundingBox.minY)*self.myPhoto.bounds.height-size.height)
                    let redLayer = CAShapeLayer()
                    redLayer.frame = CGRect(origin: origin, size: size)
                    redLayer.borderColor = UIColor.red.cgColor
                    redLayer.borderWidth = 2
                    self.myPhoto.layer.addSublayer(redLayer)
                    
                    
                })
            }
        }
        
        let handler = VNImageRequestHandler(ciImage:ciImage)
        do{
            try handler.perform([request])
        }catch{
            print("HATA!!!")
        }
        
        
        
    }
    @IBAction func degistir(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
}


extension ViewController:UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
            return
        }
        myPhoto.image = image
        self.dismiss(animated: true, completion: nil)
        faceDetection(image: image)
    }
}


